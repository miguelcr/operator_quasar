import MainPage from 'layouts/MainPage';
import Operator from 'pages/Operator';

import DataTable from 'components/DataTable';
import CreateEvent from 'components/CreateEvent';
import Login from 'pages/Login';

const routes = [{
    path: '/',
    component: MainPage,
    children: [{
      path: '',
      component: Login
    }]
  },
  {
    path: '/Operator',
    component: MainPage,
    children: [{
        path: '',
        component: Operator
      }
    ]
  },

  {
    path: '/Events',
    component: MainPage,
    children: [{
        path: '',
        component: DataTable
      }
    ]
  },

  {
    path: '/CreateCoiso',
    component: MainPage,
    children: [{
        path: '',
        component: Operator
      }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () =>
      import('pages/Error404.vue')
  }
]

export default routes
